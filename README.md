RWTHCli
=======
![License](https://img.shields.io/badge/license-GPLv2-green.svg)
![Platforms](https://img.shields.io/badge/platform-linux--64%7Cwin--64-lightgrey.svg)

A command line tool to synchronize your RWTH Aachen learning materials with your local files

## Features
RWTHCli aims to be a fully-featured tool to access numerous online services of the RWTH Aachen from the comfort of your terminal. At this point, it only lets you list courses you are currently registered at on the L2P and is hardcoded to download pdfs from ss18. 

PRs are encouraged.

## What you need
- A `client-id`
- [Rust](https://www.rust-lang.org/en-US/)

## Where do I get a client ID?

([Stolen from sour-dough](https://github.com/sour-dough/rwth-marks/#where-do-i-get-a-client-id))
### Officially

Ask the RWTH ITC (but they will probably deny it)

### Some random trivia

- A client id ends with `.app.rwth-aachen.de` or `.apps.rwth-aachen.de`.
- The [RWTH Android app](https://play.google.com/store/apps/details?id=de.rwth_aachen.rz.rwthapp) contains such a client ID.
- You can download android APKs by entering a Play Store URL at [apkpure.com](https://apkpure.com).
- APK files are actually ZIP files
- On a Unix-like operating system, you can search for a string `foobar` in all files in a directory and its subdirectories using `grep -R foobar .`
- Other progam's binaries such as "SyncMyL2P" also contain an API key
- On a Unix-like operating system, you can show strings contained in a binary file with `strings`.
