# v0.1.3

- its now possible to differentiate between Moodle and the L2P by typing `$ rwth_cli sync/courses moodle/l2p`
- syncing all l2p files at once has been moved to the command `$ rwth_cli sync l2p`
- extended moodle functionality (fixes #6):
	- `$ rwth_cli courses` now lists moodle courses. Leaving the command that way defaults to showing the current semester but it can be specified by adding one or multiple semesters (i.e. `$ rwth_cli courses ss19` or `$ rwth_cli courses ss18 ss19`).
	- `$ rwth_cli sync moodle` allows you to browse through the available topics and files in a learning room.
		- `$ rwth_cli sync moodle <course id>` lists the available topics in the course, `$ rwth_cli sync moodle <course id> <topic name>` downloads all available files belonging to the specified topic
- moved authentication data-check so that is it only invoked when subcommands that are actually interacting with the api are called. Fixes #5

# v0.1.2

- fixed versioning fuckup
- fixed Bug in `is_token_still_valid`. Function allways returned `false` which caused the tokens to be updated with every execution.
- implemented basic communication with moodle. `rwth_cli courses` now also shows one's enrolled moodle-courses for the current semester

# v0.0.2

- added progress bars for l2p file downloads
- since version 0.10.0, reqwest has been changed so that the default client uses async while this implementation relies on the blocking client. The code has been tweaked to accomodate for this change by explicitly calling the blocking client
	- additionally, the code now uses Rust 2018

# v0.1.1

- support pinging the l2p api, l2p authentication, listing courses and experimental file download (hardcoded to ss18)
