mod api;
mod config_access;
mod user_interaction;

extern crate serde;
extern crate serde_json;
extern crate toml;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate serde_derive;

use api::ltp::*;
use config_access::*;
use user_interaction::clap_subcommands::*;

use clap::{App, AppSettings, Arg, SubCommand};

fn main() {
    let mut config_data = toml_from_config();
    
    let matches = App::new(crate_name!())
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .about("A command line tool to synchronize your learning materials with your local files.")
        .version(crate_version!())
        .subcommand(
            SubCommand::with_name("courses").about("Lists your courses")
            .subcommand(
                SubCommand::with_name("l2p").about("List your L2P courses")
                .arg(Arg::with_name("semester").help("Specify the semester (i.e. ss19)").takes_value(true).multiple(true))
            )
            .subcommand(
                SubCommand::with_name("moodle").about("List your moodle courses")
                .arg(Arg::with_name("semester").help("Specify the semester (i.e. ss19)").takes_value(true).multiple(true))
            )
        )
        .subcommand(SubCommand::with_name("ping").about("Pings the L2P"))
        .subcommand(
            SubCommand::with_name("sync").about("Downloads files from your courses and creates an according folder structure")
            .subcommand(
                SubCommand::with_name("l2p").about("download your l2p files")
                .arg(Arg::with_name("course id and folder").help("specify from where to download").takes_value(true).multiple(true))
            )
            .subcommand(
                SubCommand::with_name("moodle").about("download your moodle files")
                .arg(Arg::with_name("course id and folder").help("specify from where to download").takes_value(true).multiple(true))
            )
        )
        .get_matches();

    match matches.subcommand() {
        ("courses", Some(courses_matches)) => {
            check_and_refresh_config(&mut config_data);

            match courses_matches.subcommand() {
                ("l2p", Some(l2p_matches)) => {
                    match l2p_matches.values_of("semester") {
                        Some(values) => {
                            let semesters = values.collect::<Vec<_>>();

                            if semesters.contains(&"all") {
                                list_courses(view_all_course_info());
                            } else {
                                for semester in semesters {
                                    list_courses(view_all_course_info_by_semester(semester));
                                }
                            }
                        }

                        // When no further specification is given, default to showing the current semester
                        None => {
                            list_courses(view_all_course_info_by_current_semester());
                        }
                    }
                }

                ("moodle", Some(moodle_matches)) => {
                    match moodle_matches.values_of("semester") {
                        Some(values) => {
                            let semesters = values.collect::<Vec<_>>();

                            if semesters.contains(&"all") {
                                let mut x = 0;
                                while semester_has_courses("", &x.to_string()) {
                                    list_courses_moodle("", &x.to_string());
                                    println!("");
                                    x = x+1;
                                }
                            } else {
                                for semester in semesters {
                                    if semester_has_courses(semester, "") {
                                        list_courses_moodle(semester, "");
                                    }
                                }
                                println!("");
                            }
                        }

                        // When no further specification is given, default to showing the current semester
                        None => {
                            list_courses_moodle("", "0");
                        }
                    }
                }

                // default to showing all l2p and current moodle courses when no further command is given
                _ => {
                    list_courses(view_all_course_info());
                    list_courses_moodle("", "0");

                }
            }
        }

        ("ping", _) => {
            check_and_refresh_config(&mut config_data);
            ping()
        }

        ("sync", Some(sync_matches)) => {
            check_and_refresh_config(&mut config_data);

            match sync_matches.subcommand() {
                ("l2p", Some(l2p_matches)) => {
                    sync_all(view_all_course_info());
                }

                ("moodle", Some(moodle_matches)) => {
                    match moodle_matches.values_of("course id and folder") {
                        Some(values) => {
                            let info = values.collect::<Vec<_>>();

                            if info.len() > 1 {
                                download_topic_moodle(info[0], &info[1..].join(" "));
                            } else {
                                list_topics_in_course(info[0]);
                            }
                        }

                        None => {
                            println!("Specify which folder you want to download");
                        }
                    }
                }

                _ => {
                    println!("No service specified");
                }
            }
        }

        _ => {
            println!("The subcommand was not recognized");
        }
    }
}
