extern crate ansi_term;
extern crate indicatif;
extern crate reqwest;
extern crate serde_json;

use self::ansi_term::{Color, Style};
use self::indicatif::{ProgressBar, ProgressStyle};
use self::reqwest::header::CONTENT_LENGTH;
use self::serde_json::Value as JsonValue;
use crate::api::ltp::*;
use crate::api::moodle::*;
use std::fs::{File, create_dir_all};
use std::io::copy;
use std::io::Read;

pub fn list_courses(courses_json: JsonValue) {
    let courses = courses_json["dataSet"].as_array().unwrap();
    let semesters: Vec<String> = get_semesters(courses);

    for semester in semesters {
        println!("{}:", Style::new().bold().paint(&semester));
        for course in courses {
            if course["semester"].as_str().unwrap() == &semester {
                println!(
                    "\t{} - {}:\n\t{}",
                    course["courseTitle"].as_str().unwrap(),
                    course["description"].as_str().unwrap(),
                    Color::Blue
                        .paint(course["url"].as_str().unwrap())
                        .to_string()
                );
            }
        }
        println!("");
    }
}

pub fn list_courses_moodle(semester: &str, semester_offset: &str) {
    let courses_json = moodle_get_my_enrolled_courses(semester, semester_offset);
    let courses = courses_json["Data"].as_array().unwrap();

    println!("Moodle courses for {}", courses[0]["category"]["idnumber"].as_str().unwrap());

    for course in courses {
        println!("\t{}, {}:\n\t{}",
            course["shortName"].as_str().unwrap(),
            course["id"].as_i64().unwrap(),
            Color::Blue.paint(course["url"].as_str().unwrap()).to_string());
    }
}

pub fn semester_has_courses(semester: &str, semester_offset: &str) -> bool {
    let courses_json = moodle_get_my_enrolled_courses(semester, semester_offset);

    match courses_json["Data"].as_array() {
        Some(x) => { x.len() != 0 }
        None => { false }
    }
}

pub fn list_topics_in_course(courseid: &str) {
    let files_json = moodle_get_all_files_of_course(courseid);
    let files = files_json["Data"].as_array().unwrap();
    let mut topics = Vec::new();

    for file in files {
        let topicname = file["topicname"].as_str().unwrap();
        if !topics.contains(&topicname) {
            topics.push(&topicname)
        }
    }

    println!("Topics in {} ({}):", get_course_name_by_id_moodle(courseid), courseid);

    for topic in topics {
        println!("\t{}", topic);
    }
}

pub fn list_files_in_course_topic(courseid: &str, topicname: &str) {
    let files_json = moodle_get_files_of_course_from_topic(courseid, topicname);
    let files = files_json["Data"].as_array().unwrap();

    println!("Files in {} ({}):", topicname, get_course_name_by_id_moodle(courseid));
    
    for file in files {
        println!("\t{}\n\t{}",
            file["filename"].as_str().unwrap(),
            file["downloadUrl"].as_str().unwrap());
    }
}

pub fn get_course_name_by_id_moodle(courseid: &str) -> String {
    let course_json = moodle_get_my_enrolled_course_by_id(courseid);
    
    course_json["Data"]["shortName"].as_str().unwrap().to_string()
}

pub fn get_course_semester_by_id_moodle(courseid: &str) -> String {
    let course_json = moodle_get_my_enrolled_course_by_id(courseid);
    
    course_json["Data"]["category"]["idnumber"].as_str().unwrap().to_string()
}

pub fn sync_all(courses_json: JsonValue) {
    let courses = courses_json["dataSet"].as_array().unwrap();
    let semesters: Vec<String> = get_semesters(courses);
    let mut learning_materials: JsonValue;
    let mut path;

    for semester in semesters {
        for course in courses {
            path = "".to_string();
            path.push_str(&semester);
            if course["semester"].as_str().unwrap() == &semester && &semester == "ss18" {
                path.push_str("/");
                path.push_str(course["courseTitle"].as_str().unwrap());
                create_dir_all(&path).unwrap();

                learning_materials =
                    view_all_learning_materials(course["uniqueid"].as_str().unwrap());

                for material in learning_materials["dataSet"].as_array().unwrap() {
                    if !material["isDirectory"].as_bool().unwrap()
                        && material["fileInformation"]["fileName"]
                            .as_str()
                            .unwrap()
                            .contains(".pdf")
                    {
                        download_file(
                            material["fileInformation"]["fileName"].as_str().unwrap(),
                            course["uniqueid"].as_str().unwrap(),
                            material["fileInformation"]["downloadUrl"].as_str().unwrap(),
                            &path,
                        );
                    }
                }
            }
        }
    }
}

fn get_semesters(courses_array: &Vec<JsonValue>) -> Vec<String> {
    let mut semesters: Vec<String> = Vec::new();

    for course in courses_array {
        if !semesters.contains(&course["semester"].as_str().unwrap().to_string()) {
            semesters.push(course["semester"].as_str().unwrap().to_string());
        }
    }

    semesters
}

fn download_handler_moodle(path: &str, file_name: &str, download_url: &str) {
    let file_path = path.to_owned() + "/" + file_name;
    let mut resp = moodle_download_file(file_name, download_url);

    if resp.status().is_success() {
        //let headers = resp.headers().clone();

        /*let ct_len = headers
            .get(CONTENT_LENGTH)
            .unwrap()
            .to_str()
            .unwrap()
            .parse::<u64>()
            .ok();*/

        println!("{}", format!("Saving to: {}", file_path));

        let chunk_size = 1024usize;
        /* let chunk_size = match ct_len {
            Some(x) => x as usize / 99,
            None => 1024usize, // default chunk size
        }; */

        let mut buf = Vec::new();

        //let bar = create_progress_bar(file_name, ct_len);

        loop {
            let mut buffer = vec![0; chunk_size];
            let bcount = resp.read(&mut buffer[..]).unwrap();
            buffer.truncate(bcount);
            if !buffer.is_empty() {
                buf.extend(buffer.into_boxed_slice().into_vec().iter().cloned());
                //bar.inc(bcount as u64);
            } else {
                break;
            }
        }

        //bar.finish();
        
        let mut file = File::create(file_path).unwrap();
        copy(&mut (&mut buf).as_slice(), &mut file).unwrap();
    }
}

fn create_progress_bar(msg: &str, length: Option<u64>) -> ProgressBar {
    let bar = match length {
        Some(len) => ProgressBar::new(len),
        None => ProgressBar::new_spinner(),
    };

    bar.set_message(msg);
    match length.is_some() {
        true => bar
            .set_style(ProgressStyle::default_bar()
                .template("[{wide_bar:.cyan/blue}] {spinner:.green} [{elapsed_precise}]\n{bytes}/{total_bytes} eta: {eta} {wide_msg}")),
        false => bar.set_style(ProgressStyle::default_spinner()),
    };

    bar
}

pub fn download_topic_moodle(courseid: &str, topicname: &str) {
    let files_json = moodle_get_files_of_course_from_topic(courseid, topicname);
    let files = files_json["Data"].as_array().unwrap();
    let directory_name = &(get_course_semester_by_id_moodle(courseid) + "/" + &get_course_name_by_id_moodle(courseid) + "/" + topicname);

    for file in files {
        let filetopic = file["topicname"].as_str().unwrap();
        if topicname == filetopic {
            create_dir_all(directory_name).unwrap();
            download_handler_moodle(
                directory_name,
                file["filename"].as_str().unwrap(),
                file["downloadUrl"].as_str().unwrap()
            )
        }
    }
}
