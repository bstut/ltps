extern crate serde;
extern crate serde_json;

use crate::api::oauth::{is_token_still_valid, refresh_access_data, setup_access};

use std::fs::{write, File};
use std::io::prelude::Read;
use toml::{to_string, Value};

pub fn toml_from_config() -> Value {
    let error_msg = "\nThe file must follow this scheme:\nclient_id = \"<your-client-id>\"";

    let mut config_file = File::open(".rwth.toml")
        .expect(
            &("You don't have a configuration file. Create a file named .rwth.toml containing your client id.".to_owned() + &error_msg)
        );
    let mut config_string = String::new();
    config_file
        .read_to_string(&mut config_string)
        .expect("something went wrong reading the file");

    let config_data = match config_string.parse::<Value>() {
        Ok(val) => val,

        Err(_) => panic!("Your configuration file is malformatted.".to_owned() + &error_msg),
    };

    config_data
}

pub fn check_and_refresh_config(config_data: &mut Value) {
    let mut config_is_new = false;

    match config_data.get("client_id") {
        Some(_) => (),

        None => {
            panic!("Your configuration file does not contain a client-id or is malformatted.");
        }
    };

    match config_data.get("access_data") {
        Some(_) => (),
        None => {
            setup_access(&get_client_id());
            config_is_new = true;
        }
    }

    if !config_is_new {
        if !is_token_still_valid(&get_client_id(), &get_access_token()) {
            let access_token = refresh_access_data(&get_client_id(), &get_refresh_token());

            set_access_token(&access_token);
        }
    }
}

pub fn toml_to_config(data: Value) {
    write(".rwth.toml", to_string(&data).unwrap()).expect("Couldn't write toml to config");
}

fn get_client_id() -> String {
    let data = toml_from_config();

    data["client_id"].as_str().unwrap().to_owned()
}

fn get_access_token() -> String {
    let data = toml_from_config();

    data["access_data"]["access_token"]
        .as_str()
        .unwrap()
        .to_owned()
}

#[allow(unused)]
fn get_token_type() -> String {
    let data = toml_from_config();

    data["access_data"]["token_type"]
        .as_str()
        .unwrap()
        .to_owned()
}

#[allow(unused)]
fn get_expiration() -> String {
    let data = toml_from_config();

    data["access_data"]["expires_in"]
        .as_str()
        .unwrap()
        .to_owned()
}

fn get_refresh_token() -> String {
    let data = toml_from_config();

    data["access_data"]["refresh_token"]
        .as_str()
        .unwrap()
        .to_owned()
}

#[allow(unused)]
fn set_client_id(client_id: &str) {
    let mut data = toml_from_config();

    data["client_id"] = Value::String(client_id.to_owned());

    toml_to_config(data);
}

#[allow(unused)]
fn set_access_token(access_token: &str) {
    let mut data = toml_from_config();

    data["access_data"]["access_token"] = Value::String(access_token.to_owned());

    toml_to_config(data);
}

#[allow(unused)]
fn set_token_type() {
    unimplemented!();
}

#[allow(unused)]
fn set_expiration() {
    unimplemented!();
}

#[allow(unused)]
fn set_refresh_token(refresh_token: &str) {
    let mut data = toml_from_config();

    data["access_data"]["refresh_token"] = Value::String(refresh_token.to_owned());

    toml_to_config(data);
}
