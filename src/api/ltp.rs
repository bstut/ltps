extern crate indicatif;
extern crate reqwest;
extern crate serde_json;
extern crate toml;

use self::indicatif::{ProgressBar, ProgressStyle};
use self::reqwest::blocking::{Client, Response};
use self::reqwest::header::CONTENT_LENGTH;
use self::serde_json::from_str;
use self::serde_json::Value as JsonValue;
use super::oauth::get_access_token;
use std::fs::File;
use std::io::copy;
use std::io::prelude::*;

static LTP_BASE: &'static str =
    "https://www3.elearning.rwth-aachen.de/_vti_bin/l2pservices/api.svc/v1/";

fn ltp_get_request(method: &str, params: &[(&str, &str)]) -> Response {
    let response;
    let client = Client::new();
    let mut request_url = LTP_BASE.to_owned() + method + "?accessToken=" + &get_access_token();

    for param in params {
        request_url.push_str(&"&");
        request_url.push_str(param.0);
        request_url.push_str("=");
        request_url.push_str(param.1);
    }

    response = client.get(&request_url).send().unwrap();

    response
}

#[allow(unused)]
pub fn view_user_info() {
    let response = ltp_get_request("viewUserInfo", &[]).text();
}

pub fn ping() {
    let response = ltp_get_request("Ping", &[("p", "Pong")]);

    if response.status().is_success() {
        println!("Pong");
    } else {
        println!("{}", response.text().unwrap());
    }
}
#[allow(unused)]
pub fn view_all_course_events() -> JsonValue {
    let mut response = ltp_get_request("viewAllCourseEvents", &[]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn view_all_course_info() -> JsonValue {
    let response = ltp_get_request("viewAllCourseInfo", &[]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn view_all_course_info_by_current_semester() -> JsonValue {
    let response = ltp_get_request("viewAllCourseInfoByCurrentSemester", &[]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn view_all_course_info_by_semester(semester: &str) -> JsonValue {
    let params = [("semester", semester)];
    let response = ltp_get_request("viewAllCourseInfoBySemester", &params);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn view_all_learning_materials(cid: &str) -> JsonValue {
    let params = [("cid", cid)];

    let response = ltp_get_request("viewAllLearningMaterials", &params);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn download_file(file_name: &str, cid: &str, download_url: &str, path: &str) {
    let file_path = path.to_owned() + "/" + file_name;
    let params = [("cid", cid), ("downloadUrl", download_url)];
    let mut resp = ltp_get_request(&("downloadFile/".to_owned() + file_name), &params);

    if resp.status().is_success() {
        let headers = resp.headers().clone();
        let ct_len = headers
            .get(CONTENT_LENGTH)
            .unwrap()
            .to_str()
            .unwrap()
            .parse::<u64>()
            .ok();

        println!("{}", format!("Saving to: {}", file_path));

        let chunk_size = match ct_len {
            Some(x) => x as usize / 99,
            None => 1024usize, // default chunk size
        };

        let mut buf = Vec::new();

        let bar = create_progress_bar(file_name, ct_len);

        loop {
            let mut buffer = vec![0; chunk_size];
            let bcount = resp.read(&mut buffer[..]).unwrap();
            buffer.truncate(bcount);
            if !buffer.is_empty() {
                buf.extend(buffer.into_boxed_slice().into_vec().iter().cloned());
                bar.inc(bcount as u64);
            } else {
                break;
            }
        }

        bar.finish();
        println!("");

        println!("{}", file_path);
        let mut file = File::create(file_path).unwrap();
        copy(&mut (&mut buf).as_slice(), &mut file).unwrap();
    }
}

fn create_progress_bar(msg: &str, length: Option<u64>) -> ProgressBar {
    let bar = match length {
        Some(len) => ProgressBar::new(len),
        None => ProgressBar::new_spinner(),
    };

    bar.set_message(msg);
    match length.is_some() {
        true => bar
            .set_style(ProgressStyle::default_bar()
                .template("[{wide_bar:.cyan/blue}] {spinner:.green} [{elapsed_precise}]\n{bytes}/{total_bytes} eta: {eta} {wide_msg}")),
        false => bar.set_style(ProgressStyle::default_spinner()),
    };

    bar
}
