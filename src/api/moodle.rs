extern crate reqwest;
extern crate serde_json;

use self::reqwest::blocking::{Client, Response};
use self::serde_json::from_str;
use self::serde_json::Value as JsonValue;
use super::oauth::get_access_token;

static MOODLE_BASE: &'static str =
    "https://moped.ecampus.rwth-aachen.de/proxy/api/v2/eLearning/Moodle/";

fn moodle_get_request(method: &str, params: &[(&str, &str)]) -> Response {
    let response;
    let client = Client::new();
    let mut request_url = MOODLE_BASE.to_owned() + method + "?";

    for param in params {
        request_url.push_str(param.0);
        request_url.push_str("=");
        request_url.push_str(param.1);
        request_url.push_str(&"&");
    }

    request_url = request_url + "token=" + &get_access_token();

    response = client.get(&request_url).send().unwrap();

    response
}

pub fn moodle_get_all_files_of_course(courseid: &str) -> JsonValue {
    let response = moodle_get_request("GetFiles", &[("courseid", courseid)]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn moodle_get_files_of_course_from_topic(courseid: &str, topicname: &str) -> JsonValue {
    let params = [("courseid", courseid), ("topicname", topicname)];
    let response = moodle_get_request("GetFiles", &params);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn moodle_get_my_enrolled_courses(semester: &str, semester_offset: &str) -> JsonValue {
    let params = [("semester", semester), ("semesterOffset", semester_offset)];

    let response = moodle_get_request("GetMyEnrolledCourses", &params);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn moodle_get_my_enrolled_course_by_id(courseid: &str) -> JsonValue {
    let response = moodle_get_request("GetMyEnrolledCourseById", &[("courseid", courseid)]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();

        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn moodle_download_file(file_name: &str, download_url: &str) -> Response {
    let params = [("downloadurl", download_url)];

    let response = moodle_get_request(&("DownloadFile/".to_owned() + file_name), &params);

    response
}
