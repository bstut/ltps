extern crate reqwest;
extern crate toml;
extern crate serde_json;

use self::reqwest::blocking::{Client, Response};
use serde_json::{from_str, Value};
use toml::Value as TomlValue;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::{thread, time};

static OAUTH_BASE: &'static str = "https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/";

#[derive(Debug, Serialize, Deserialize)]
/// used to store the json that holds the information neccessary for app authorization
/// see: https://oauth.campus.rwth-aachen.de/doc/#obtaining-a-user-code
pub struct AuthData {
    pub device_code: String,
    pub user_code: String,
    pub verification_url: String,
    pub expires_in: u64,
    pub interval: u64,
}

#[derive(Debug, Serialize, Deserialize)]
/// used to store the json that is being returned when the user has granted access to the application
/// see: https://oauth.campus.rwth-aachen.de/doc/#obtaining-access-and-refresh-tokens
pub struct AccessData {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: u64,
    pub refresh_token: String,
}

impl AuthData {
    pub fn get_authorization_url(&self) -> String {
        let str =
            "https://oauth.campus.rwth-aachen.de/manage/?q=verify&d=".to_owned() + &self.user_code;

        str
    }

    pub fn get_access_data(&self, client_id: &str) -> AccessData {
        let mut response;
        let params = [
            ("client_id", client_id),
            ("code", &self.device_code),
            ("grant_type", "device"),
        ];

        loop {
            response = oauth_post("token", &params).text().unwrap();

            if response.contains("error") {
                thread::sleep(time::Duration::from_secs(self.interval));
            } else {
                break;
            }
        }

        let access_data: AccessData = from_str(&response).unwrap();

        access_data
    }
}

impl AccessData {
    pub fn update_config(&self) {
        let mut config = OpenOptions::new()
            .write(true)
            .append(true)
            .open(".rwth.toml")
            .unwrap();

        writeln!(config, "\n\n[access_data]").unwrap();
        let toml = toml::to_string(&self).unwrap();
        writeln!(config, "{}", toml).unwrap();
    }
}

fn oauth_post(specifier: &str, params: &[(&str, &str)]) -> Response {
    let request_url = &(OAUTH_BASE.to_owned() + specifier);

    let client = Client::new();
    let response = client.post(request_url).form(params).send().unwrap();

    response
}

pub fn setup_access(client_id: &str) {
    let auth_data = get_authorization_data(&client_id);
    println!(
        "Open this URL and authorize this app, then wait a few seconds:\n{}",
        auth_data.get_authorization_url()
    );

    let access_data: AccessData = auth_data.get_access_data(&client_id);
    access_data.update_config();
}

/// sends a request to RWTH Aachen Oauth2 Endpoint
/// returns a struct containing data neccessary for the user to grant
/// access to our application
pub fn get_authorization_data(client_id: &str) -> AuthData {
    let params = [
        ("client_id", client_id),
        ("scope", "l2p2013.rwth userinfo.rwth moodle.rwth"),
    ];

    let response: String = oauth_post("code", &params).text().unwrap();

    let auth_request_body: AuthData = from_str(&response).unwrap();

    auth_request_body
}

pub fn is_token_still_valid(client_id: &str, access_token: &str) -> bool {
    let params = [("client_id", client_id), ("access_token", access_token)];

    let response = oauth_post("tokeninfo", &params).text().unwrap();
    let data: Value = from_str(&response).unwrap();

    !(data["expires_in"].as_u64().unwrap() == 0)
}

/// sends request for a new access token and returns it
pub fn refresh_access_data(client_id: &str, refresh_token: &str) -> String {
    let params = [
        ("client_id", client_id),
        ("refresh_token", refresh_token),
        ("grant_type", "refresh_token"),
    ];

    let response = oauth_post("token", &params).text().unwrap();
    let data: Value = from_str(&response).unwrap();

    data["access_token"].as_str().unwrap().to_owned()
}

/// If the access is already granted, the api access is granted by the access token
/// which we store in .rwth.toml. This token is read and returned by this function
pub fn get_access_token() -> String {
    let mut config_string = String::new();
    let mut config_file =
        File::open(".rwth.toml").expect("Something went wrong opening .rwth.toml");
    config_file
        .read_to_string(&mut config_string)
        .expect("something went wrong reading the file");
    let config_data = config_string.parse::<TomlValue>().unwrap();

    config_data["access_data"]["access_token"]
        .as_str()
        .unwrap()
        .to_owned()
}
